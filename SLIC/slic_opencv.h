//
//  slic_opencv.h
//  testSLIC
//
//  Created by Nina Cai on 05/06/2015.
//  Copyright (c) 2015 Nina Cai. All rights reserved.
//

#ifndef __testSLIC__slic_opencv__
#define __testSLIC__slic_opencv__

#include "SLIC.h"

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class mySLIC
{
public:

    // use initialization list to initialize constants and showSeg to false
    mySLIC() :MAX_STROKE_RADIUS(200),
        MIN_STROKE_RADIUS(5),
        DEFULT_STROKE_RADIOUS(5),
        MAX_W(1200),
        MAX_H(1200),
        showSeg(false)
    {}

    ~mySLIC();
    /* opencv wrapper for SLIC super pixel algorithm
    * input:
    *      src: input image
    *      k:   number of super pixels
    *      m:   compactness, defualt = 10
    *
    * output:
    *      dst:   output image with super pixel labeled
    *      label: each pixel corresponding's label, same size of dst
    */
    void superpixel(const Mat& src, Mat& dst, Mat& label, int k, double m = 10);

    /* start an UI for user to select super pixel
    * input:
    *      src: input image
    *      label: input super pixel labels
    *      method: 0: single cursor - only select one super pixel at one click
    *              1: stroke (defualt) - could select multiple super pixel at one click
    *
    * output:
    *      dst: image with only selected super pixels
    *
    * main selection, de-selection and visualization algorithm implemented by mouseEvent()
    */
    void segmentui(Mat& dst, int method = 1);

    // handle mousemove event for visualizing current super pixel
    void showSuperPixel(int r, int c);
    // handle left_mousedown event for selecting current super pixel
    void selectSuperPixel(int r, int c);
    // handle right_mousedown event for de-selecting current super pixel
    void deselectSuperPixel(int r, int c);

    /// New UI for segmentation: use stroke to select
    // mousemove
    void showStroke(int r, int c);
    // left_mouseup
    void selectByStroke(int r, int c);
    // left_mousedown
    void markByStroke(int r, int c);

    void clearStroke();

    // make stroke smaller by pressing '-' or '_'
    void shrinkStroke();
    // make stroke larger by pressing '+' or '='
    void enlargeStroke();

    bool mDown; // indicating whether current mousedown
    Point2i lastPos;

private:
    void getSuperPixel(int x, int y);
    void getHighlight(int x, int y);
    void getMask(int x, int y);
    void setColor(int r, int g, int b);

    int width;
    int height;
    int* klabels;
    int numlabels;
    int lastShow;
    int lastSelect;

    Mat src_;       // a copy of original input image
    Mat label_;     // each pixel's label, indicating which super pixel it belongs to
    Mat superpixel_;    // original input image with highlighted super pixel boundaries
    Mat seg_;       // for storing the segmentation result and to be returned to dst
    Mat mask_;      // for selectSuperPixel, contain the mask for copying pixels to selected segments
    Mat highlight_; // for showSuperPixel, contain the current highlighted super pixel
    Mat seg_highlight_; // for highlighting selected super pixels
    Mat blend_;     // current blending image for showing highlighted super pixel
    Mat stroke_;	// current stroke
    Mat mask_result;

    vector<int> pixelCount;     // count how many pixels in each super pixel
    vector<int> halfPixelCount; // half of count of pixelCount, for comparison
    int strokeRadius;
    const int MAX_STROKE_RADIUS;	// = 100;
    const int MIN_STROKE_RADIUS;	// = 5;
    const int DEFULT_STROKE_RADIOUS;// = 15;
    int k_;

    const int MAX_W;
    const int MAX_H;
    bool showSeg;
};

#endif /* defined(__testSLIC__slic_opencv__) */