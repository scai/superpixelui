#include "slic_opencv.h"
#include "SeedsRevised.h"
#include "Tools.h"

#include <cmath>

extern mySLIC myslic;

mySLIC::~mySLIC(){
    if (klabels){
        delete[] klabels;
    }
}

void mySLIC::superpixel(const Mat& _src, Mat& dst, Mat& label, int k, double m /* = 10 */)
{
    // check source image dimension and rescale it
    Mat src = _src.clone();
    label = Mat(src.size(), CV_32S);

    if (src.cols > MAX_W)
    {
        double scale = (double)MAX_W / src.cols;
        resize(src, src, Size(0, 0), scale, scale);
    }
    else if (src.rows > MAX_H)
    {
        double scale = (double)MAX_H / src.rows;
        resize(src, src, Size(0, 0), scale, scale);
    }

    /* preprocess inputs to required formats */
    width = src.size().width;
    height = src.size().height;
    //assert(width > 0 && height > 0 && src.type() == 8UC3);
    assert(width > 0 && height > 0 && src.type() == CV_8UC3);

    // initialize pixelCount
    pixelCount.clear();
    pixelCount.resize(k, 0);
    halfPixelCount.clear();
    halfPixelCount.resize(k, 0);

//----------------------------------------------------SLIC Algorithm------------------------------------------------------------------------------------
#ifdef DEBUG_SLIC
    // convert OpenCV's BGR format to required ARGB // BGR[0] -> ARGB[3]  // BGR[1] -> ARGB[2] // BGR[2] -> ARGB[1] // BGR[0or1or2] -> AGRB[0]
    Mat argb(src.size(), CV_8UC4);
    int from_to[] = { 0, 3, 1, 2, 2, 1, 0, 0 };
    mixChannels(&src, 1, &argb, 1, from_to, 4);

    /* run SLICO algorithm*/
    unsigned int* pbuff = (unsigned int*)argb.data;
    klabels = new int[width*height];
    int numlabels(0);
    SLIC segment;
    segment.PerformSLICO_ForGivenK(pbuff, width, height, klabels, numlabels, k, m);
    // Draw boundaries around segments
    segment.DrawContoursAroundSegments(pbuff, klabels, width, height, 0xff0000);
    //    segment.DrawContoursAroundSegmentsTwoColors(pbuff, klabels, width, height);

    /* save results to opencv's data structures */     // save image with marked super pixels
    unsigned char* outdata = (unsigned char*)pbuff;
    Mat out(src.size(), CV_8UC4, outdata);
    // switch channels from ARGB to BGR
    dst = Mat(src.size(), CV_8UC3);
    int from_to_out[] = { 1, 2, 2, 1, 3, 0 };
    mixChannels(&out, 1, &dst, 1, from_to_out, 3);


    // save labels and count how many pixels in each super pixel
    
    for (int i = 0; i < height; i++)
    for (int j = 0; j < width; j++)
    {
        label.at<int>(i, j) = klabels[i*width + j];
        pixelCount[klabels[i*width + j]]++;
    }

    for (int i = 0; i < k; i++)
        halfPixelCount[i] = pixelCount[i] / 2;

    //if (klabels) delete[] klabels;
#else if
//-------------------------------------------------------SLIC Algo END---------------------------------------------------------------------------------
//-------------------------------------------------------SEEDS Algo START---------------------------------------------------------------------------------
    int superpixels = k; //400; //desired number of superpixels
    int numberOfBins = 5; // number of bins used for color histogram
    int neighborhoodSize = 1; //neighborhood size used for smoothing prior
    float minimumConfidence = 0.1; //minimum confidence used for block update
    float spatialWeight = 0.25; // spatial weight
	int iterations = 5;//2; // iterations at each level
    SEEDSRevisedMeanPixels seeds(src, superpixels, numberOfBins, neighborhoodSize, minimumConfidence, spatialWeight);
    seeds.initialize();
    seeds.iterate(iterations);
    int bgr[] = { 0, 0, 204 };
    int ** labelsArray = seeds.getLabels();
    //cout << sizeof (labelsArray) / sizeof(int*) << endl;
    //Mat label(src.rows, src.cols, CV_8UC1);
//#pragma omp parallel for
    for (int i = 0; i < src.rows; i++){
        for (int j = 0; j < src.cols; j++){
            //cout << i << ", " << j << endl;
            label.at<int>(i, j) = labelsArray[i][j];
            pixelCount[label.at<int>(i, j)]++;
        }
    }
//#pragma omp parallel for
    for (int i = 0; i < superpixels; i++)
        halfPixelCount[i] = pixelCount[i] / 2;

    cv::Mat contourImage = Draw::contourImage(seeds.getLabels(), src, bgr);
//-------------------------------------------------------SEEDS Algo END---------------------------------------------------------------------------------
#endif
    //keep a copy of src and label
    src_ = src.clone();
    label_ = label;
    superpixel_ = contourImage.clone();
    k_ = superpixels;
}

void mouseEvent(int evt, int x, int y, int flags, void* param)
{
    if (evt == CV_EVENT_LBUTTONDOWN) // select and add the current super pixel to dst
    {
        myslic.mDown = true;
        myslic.selectSuperPixel(y, x);

    }
    else if (evt == CV_EVENT_MOUSEMOVE) // visualize the current super pixel
    {
        myslic.showSuperPixel(y, x);
        if (myslic.mDown)
            myslic.selectSuperPixel(y, x);
    }
    else if (evt == CV_EVENT_LBUTTONUP)
    {
        myslic.mDown = false;
    }
    else if (evt == CV_EVENT_RBUTTONDOWN)
    {
        myslic.deselectSuperPixel(y, x);
    }

}

void strokeEvent(int evt, int x, int y, int flags, void* param)
{
    if (evt == CV_EVENT_LBUTTONDOWN) // select and add the current super pixel to dst
    {
        myslic.mDown = true;
        // start marking strokes but do not calculate
        myslic.markByStroke(y, x);
    }
    else if (evt == CV_EVENT_MOUSEMOVE) // visualize the current super pixel
    {
        if (myslic.mDown)
            myslic.markByStroke(y, x);
        myslic.showStroke(y, x);
        myslic.lastPos = Point2i(x, y);
    }
    else if (evt == CV_EVENT_LBUTTONUP)
    {
        myslic.mDown = false;
        // select super pixels according to current stroke
        myslic.selectByStroke(y, x);
        myslic.clearStroke();
    }
    else if (evt == CV_EVENT_RBUTTONDOWN)
    {
        myslic.deselectSuperPixel(y, x);
    }

}
void mySLIC::markByStroke(int r, int c)
{
    circle(stroke_, Point2i(c, r), strokeRadius, Scalar(0, 255, 0), -1);
}

void mySLIC::clearStroke()
{
    stroke_ = Mat::zeros(src_.size(), CV_8UC3);
}

void mySLIC::getHighlight(int r, int c)
{
    // set highlight to current segmentation
    highlight_ = seg_highlight_.clone();
    int curlabel = label_.at<int>(r, c);
#pragma omp parallel for
    for (int row = 0; row < height; ++row)
    for (int col = 0; col < width; ++col)
    if (label_.at<int>(row, col) == curlabel)
        highlight_.at<Vec3b>(row, col)[1] = 255;

}

void mySLIC::showSuperPixel(int r, int c)
{
    // get current pixel label
    int curLabel = label_.at<int>(r, c);

    // if need updating
    if (curLabel != lastShow)
    {
        // get the current super pixel patch in green
        getHighlight(r, c);

        // blend with original image
        addWeighted(superpixel_, 0.5, highlight_, 0.5, 0.0, blend_);

        // show the blended image
        imshow("segmentUI", blend_);
        waitKey(1);

        lastShow = curLabel;
    }
}

void mySLIC::selectSuperPixel(int r, int c)
{
    //    cout << "mousedown at (" << r << "," << c << ")" << endl;

    // stupid method first: just search and assign every pixel in selected region to seg_
    // get current pixel label
    int curLabel = label_.at<int>(r, c);

    // if need updating
    if (curLabel != lastSelect)
    {
#pragma omp parallel for
        for (int row = 0; row < height; ++row)
        for (int col = 0; col < width; ++col)
        if (curLabel == label_.at<int>(row, col))
        {
            seg_.at<Vec3b>(row, col) = src_.at<Vec3b>(row, col);
            seg_highlight_.at<Vec3b>(row, col)[1] = 255;
        }

        if (showSeg)
            imshow("segment", seg_);
        imshow("segmentUI", blend_);
        waitKey(1);

        lastSelect = curLabel;
    }
}

void mySLIC::deselectSuperPixel(int r, int c)
{
    int curLabel = label_.at<int>(r, c);

#pragma omp parallel for
    for (int row = 0; row < height; ++row)
    for (int col = 0; col < width; ++col)
    if (curLabel == label_.at<int>(row, col))
    {
        seg_.at<Vec3b>(row, col) = Vec3b(0, 0, 0);
        seg_highlight_.at<Vec3b>(row, col)[1] = 0;
    }

    if (showSeg)
        imshow("segment", seg_);
    imshow("segmentUI", blend_);
    waitKey(1);

    lastSelect = -1;
    lastShow = -1;

}

void mySLIC::segmentui(Mat& dst, int method)
{
    namedWindow("segmentUI");

    switch (method) {
    case 0:
        // UI method 0: single cursor
        setMouseCallback("segmentUI", mouseEvent);
        cout << "Segmentation UI by single cursor:" << endl;
        cout << "- Move mouse to see current super pixel" << endl;
        cout << "- Press down left button to select current super pixel" << endl;
        cout << "- Press down right button to de-select current super pixel" << endl;
        cout << "- Press 'q' or 'Q' to finish segmentation" << endl;
        break;
    case 1:
        // UI option 1: stroke (defualt)
        setMouseCallback("segmentUI", strokeEvent);
        cout << "Segmentation UI by stroke:" << endl;
        cout << "Super pixels covered more than half will be selected" << endl;
        cout << "- Press down left button to select current covered super pixels" << endl;
        cout << "- Press down right button to de-select current super pixel" << endl;
        cout << "- Press '-' or '_' to decrease stroke size" << endl;
        cout << "- Press '+' or '=' to increase stroke size" << endl;
		cout << "- Press 'd' or 'D' to save the current result" << endl;
        cout << "- Press 'q' or 'Q' to finish segmentation" << endl;
        //cout << "- Press 's' or 'S' to show the current segmentation" << endl;
        break;
    }

    imshow("segmentUI", superpixel_);

    /**** some initialization *****/
    seg_ = Mat(src_.size(), CV_8UC3, Scalar(0, 0, 0));
    highlight_ = Mat(src_.size(), CV_8UC3);
    seg_highlight_ = Mat(src_.size(), CV_8UC3, Scalar(0, 0, 0));
    mask_ = Mat(src_.size(), CV_8UC1);
    stroke_ = Mat(src_.size(), CV_8UC3, Scalar(0, 0, 0));
    mask_result = Mat(src_.size(), CV_8UC3, Scalar(0, 0, 0));
    lastShow = -1;
    lastSelect = -1;
    mDown = false;
    strokeRadius = (int)floor(sqrt((double)width*height / k_));	// decided by approximately average super pixel size
    lastPos = Point2i(-1, -1);
    /**** Initialization done ****/

    while (1)
    {
        // mouse events are handled mouseEvent()

        // stop segmentUI by pressing 'q' or 'Q'
        char key = waitKey(10);
        if (key == 'q' || key == 'Q')
        {
            break;
        }
        if (key == 's' || key == 'S')
        {
            showSeg = !showSeg;
            if (showSeg)
                imshow("segment", seg_);
            else
                destroyWindow("segment");
        }
        else if (key == '-' || key == '_')
        {
            shrinkStroke();
        }
        else if (key == '+' || key == '=')
        {
            enlargeStroke();
        }
        else if (key == 'h' || key == 'H'){
            setColor(128, 0, 0);
        }
        else if (key == 'g' || key == 'G'){
            setColor(0, 255, 0);
        }
        else if (key == 'b' || key == 'B'){
            setColor(0, 0, 255);
        }
        else if (key == 'j' || key == 'J'){
            setColor(127, 127, 0);
        }
        else if (key == 'r' || key == 'R'){
            setColor(127, 127, 127);
        }
        else if (key == 'd' || key == 'D'){
            imwrite("mask_result.png", mask_result);
        }
    }

    dst = seg_.clone();
    destroyAllWindows();
}

void mySLIC::setColor(int r, int g, int b){
#pragma omp parallel for 
    for (int row = 0; row < height; ++row)
    for (int col = 0; col < width; ++col)
    if (seg_highlight_.at<Vec3b>(row, col)[1] == 128 )
    {
        mask_result.at<Vec3b>(row, col)[0] = b;
        mask_result.at<Vec3b>(row, col)[1] = g;
        mask_result.at<Vec3b>(row, col)[2] = r;
        seg_highlight_.at<Vec3b>(row, col)[0] = b;
        seg_highlight_.at<Vec3b>(row, col)[1] = g;
        seg_highlight_.at<Vec3b>(row, col)[2] = r;
    }
}

void mySLIC::shrinkStroke()
{
    strokeRadius = max(MIN_STROKE_RADIUS, strokeRadius - 1);
    showStroke(lastPos.y, lastPos.x);
}

void mySLIC::enlargeStroke()
{
    strokeRadius = min(MAX_STROKE_RADIUS, strokeRadius + 1);
    showStroke(lastPos.y, lastPos.x);
}

void mySLIC::showStroke(int r, int c)
{
    // hightlight_ = stroke_ | seg_highlight_
    add(stroke_, seg_highlight_, highlight_);
    // draw current stroke
    circle(highlight_, Point2i(c, r), strokeRadius, Scalar(0, 255, 0), -1);
    // blend with background
    addWeighted(superpixel_, 0.5, highlight_, 0.5, 0, blend_);
    imshow("segmentUI", blend_);
    waitKey(1);
}

void mySLIC::selectByStroke(int r, int c)
{
    // use current highlight_
    // count how many pixels in each super pixel are covered
    vector<int> count(k_, 0);
    vector<bool> covered(k_, false);

#pragma omp parallel for
    for (int i = 0; i < width; i++)
        for (int j = 0; j < height; j++)
            if (stroke_.at<Vec3b>(j, i)[1] == 255)
                count[label_.at<int>(j, i)]++;

#pragma omp parallel for    
    for (int i = 0; i < k_; i++)
        if (count[i] >= halfPixelCount[i])
            covered[i] = true;

    // draw the corresponding pixels to output
#pragma omp parallel for
    for (int row = 0; row < height; row++)
        for (int col = 0; col < width; col++)
            if (covered[label_.at<int>(row, col)])
            {
                seg_.at<Vec3b>(row, col) = src_.at<Vec3b>(row, col);
                seg_highlight_.at<Vec3b>(row, col)[1] = 128;
            }

    if (showSeg)
        imshow("segment", seg_);
    imshow("segmentUI", blend_);
    waitKey(1);

    count.clear();
    covered.clear();
}

