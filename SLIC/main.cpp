//
//  main.cpp
//  testSLIC
//
//  Created by Nina Cai on 05/06/2015.
//  Copyright (c) 2015 Nina Cai. All rights reserved.
//

#include <iostream>

#include <opencv2/opencv.hpp>
#include "slic_opencv.h"

using namespace std;
using namespace cv;
 
mySLIC myslic;

int main(int argc, const char * argv[])
{
    Mat src = imread("00224.jpg");

    Mat dst, label, seg;
    int superpixels = 800;
    myslic.superpixel(src, dst, label, superpixels);
    imwrite("result_superpixel.jpg", dst);
    myslic.segmentui(seg); 

    // now, "seg" stores the 'clean' image which could be passed to next stage
    imshow("final segment", seg);

    waitKey();

    return 0;
}

